//
//  CTWStackController.h
//  pagean
//
//  Created by Vijay Chandran on 23/11/15.
//  Copyright © 2015 Vijay Chandran. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTWStackView, CTWStackViewOverlay;
@protocol CTWStackSource <NSObject>
- (UIView *)stackView:(CTWStackView *)stackView viewForIndex:(NSInteger)index;
- (void)stackView:(CTWStackView *)stackView willDisplayView:(UIView *)view forIndex:(NSInteger)index;
- (NSInteger)numberOfViewsForStackView:(CTWStackView *)stackView;
@end

@interface CTWStackView : UIView {
    UISwipeGestureRecognizer *lSwipe, *rSwipe;
    UIPanGestureRecognizer *pan;
    NSInteger currentIndex, count, direction;
    UIView *currentView, *firstView, *secondView, *thirdView;
    CGPoint touchPoint, dif, currentViewOrigin;
    CTWStackViewOverlay *overlay;
}
@property (nonatomic) id<CTWStackSource> viewSource;
- (void)reload;
- (void)load:(NSInteger)index;
@end

@interface CTWStackViewOverlay : UIView
@property (nonatomic, weak) CTWStackView *stackView;
@end
