//
//  CTWStackController.m
//  pagean
//
//  Created by Vijay Chandran on 23/11/15.
//  Copyright © 2015 Vijay Chandran. All rights reserved.
//

#import "CTWStackView.h"

@implementation CTWStackView

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    [self removeGestureRecognizer:lSwipe];
    [self removeGestureRecognizer:rSwipe];
    lSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    lSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    [self addGestureRecognizer:lSwipe];
    overlay = [[CTWStackViewOverlay alloc] init];
    rSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    rSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    //[self addGestureRecognizer:rSwipe];
    currentIndex = 0;
    //[self reload];
    
    [self removeGestureRecognizer:pan];
    pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [self addGestureRecognizer:pan];
    
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.backgroundColor = [UIColor clearColor];
    //[self reload];
    overlay.frame = self.bounds;
    //[self addSubview:overlay];
    
}

- (void)swipe:(UISwipeGestureRecognizer *)gesture {
    if (gesture.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self showNext];
    } else if (gesture.direction == UISwipeGestureRecognizerDirectionRight) {
        [self showPrevious];
    }
}

- (void)reload {
    if (self.viewSource != nil) {
        count = [self.viewSource numberOfViewsForStackView:self];
        currentIndex = -1;
        [self getNext];
    }
}

- (void)load:(NSInteger)index {
    if (self.viewSource != nil) {
        count = [self.viewSource numberOfViewsForStackView:self];
        currentIndex = index - 1;
        [self getNext];
    }
}

- (void)loadView {
    if (currentIndex >= count) {
        currentIndex = count - 1;
        return;
    } else if (currentIndex < 0) {
        currentIndex = 0;
        return;
    }
    printf("\nloadview ci:%ld\n", (long)currentIndex);
    
    if (currentIndex + 1 < count) {
        [thirdView removeFromSuperview];
        thirdView = [self.viewSource stackView:self viewForIndex:currentIndex + 1];
        thirdView.frame = CGRectMake(0.0, 0.0, self.frame.size.width, self.frame.size.height);
        [self addSubview:thirdView];
    } else {
        [thirdView removeFromSuperview];
        thirdView = nil;
    }
    
    [secondView removeFromSuperview];
    secondView = [self.viewSource stackView:self viewForIndex:currentIndex];
    if (currentView == nil) {
        [self.viewSource stackView:self willDisplayView:secondView forIndex:currentIndex];
    }
    secondView.frame = CGRectMake(0.0, 0.0, self.frame.size.width, self.frame.size.height);
    [self applyShadow:secondView];
    [self addSubview:secondView];
    
    if (currentIndex - 1 >= 0) {
        [firstView removeFromSuperview];
        firstView = [self.viewSource stackView:self viewForIndex:currentIndex - 1];
        firstView.frame = CGRectMake(-self.frame.size.width, 0.0, self.frame.size.width, self.frame.size.height);
        [self addSubview:firstView];
    } else {
        [firstView removeFromSuperview];
        firstView = nil;
    }

}

- (void)getNext {
    currentIndex = currentIndex + 1;
    [self loadView];
    
}

- (void)getPrevious {
    currentIndex = currentIndex - 1;
    [self loadView];
}

- (void)showNext {
    if (secondView != nil && thirdView != nil) {
        [self applyShadow:secondView];
        [UIView animateWithDuration:0.4 animations:^{
            secondView.center = CGPointMake(-self.bounds.size.width / 1.0, self.frame.size.height / 2.0);
            //thirdView.center = CGPointMake(self.bounds.size.width / 2, thirdView.center.y);
            CGAffineTransform rot = CGAffineTransformMakeRotation(-M_PI_2);
            secondView.transform = rot;
        } completion:^(BOOL finished) {
            [self removeShadow:secondView];
            [self getNext];
            //currentView = nil;
        }];
    } else {
        [self resetViews];
    }
}

- (void)resetCurrentView {
    return;
    [UIView animateWithDuration:0.4 animations:^{
        currentView.center = currentViewOrigin;
    } completion:^(BOOL finished) {
        
        currentView.center = currentViewOrigin;
        [self removeShadow:currentView];
        currentViewOrigin = CGPointZero;
        currentView = secondView;
    }];

}

- (void)resetViews {
    if (firstView.center.x != -self.frame.size.width / 2.0 && firstView != nil) {
        [UIView animateWithDuration:0.2 animations:^{
            firstView.center = CGPointMake(-self.frame.size.width / 1.0, self.frame.size.height / 2.0);
            firstView.transform = CGAffineTransformMakeRotation(-M_PI_2);
        } completion:^(BOOL finished) {
            firstView.center = CGPointMake(-self.frame.size.width / 2.0, self.frame.size.height / 2.0);
            firstView.transform = CGAffineTransformMakeRotation(0.0);
            [self removeShadow:firstView];
        }];
    }
    if (secondView.center.x != self.frame.size.width / 2.0 && secondView != nil) {
        [UIView animateWithDuration:0.2 animations:^{
            secondView.center = CGPointMake(self.frame.size.width / 2.0, self.frame.size.height / 2.0);
            secondView.transform = CGAffineTransformMakeRotation(0.0);
        } completion:^(BOOL finished) {
            secondView.center = CGPointMake(self.frame.size.width / 2.0, self.frame.size.height / 2.0);
            secondView.transform = CGAffineTransformMakeRotation(0.0);
        }];

    }
}

- (void)pan:(UIPanGestureRecognizer *)g {
    CGPoint t = [pan translationInView:self];
    CGPoint v = [pan velocityInView:self];
    
    if (g.state == UIGestureRecognizerStateBegan) {
        currentView = nil;
    }
    else if (g.state == UIGestureRecognizerStateChanged) {
        if (currentView == nil) {
            if (t.x > 0) {
                
                
                currentView = firstView;
                if (currentView == nil) {
                    printf("\nSECOND VIEW %p\n", secondView);;
                    currentView = secondView;
                    if (currentIndex + 1 < count) {
                        [self.viewSource stackView:self willDisplayView:thirdView forIndex:currentIndex + 1];
                    }
                } else {
                    printf("\nFIRST VIEW %p\n", firstView);
                    if (currentIndex - 1 >= 0) {
                        [self.viewSource stackView:self willDisplayView:currentView forIndex:currentIndex - 1];
                    }
                    
                }
                
            } else if (t.x < 0) {
                printf("\nTHIRD VIEW %p\n", thirdView);
                currentView = secondView;
                if (currentIndex + 1 < count) {
                    [self.viewSource stackView:self willDisplayView:thirdView forIndex:currentIndex + 1];
                }
                
            }
            [self applyShadow:currentView];
        }
        CGFloat a = -M_PI_2 * (self.frame.size.width / 2.0 - currentView.center.x) / (self.frame.size.width * 1.5);
        if (currentView == firstView) {
            currentView.center = CGPointMake(-self.frame.size.width / 1.0 + t.x, self.frame.size.height / 2.0);
        } else if (currentView == secondView) {
            currentView.center = CGPointMake(self.frame.size.width / 2.0 + t.x, self.frame.size.height / 2.0);
        }
        currentView.transform = CGAffineTransformMakeRotation(a);
        
        
    } else if (g.state == UIGestureRecognizerStateEnded) {
        if (currentView == secondView) {
            if (secondView.center.x < self.frame.size.width * 0.3) {
                [self showNext];
            } else {
                [self resetViews];
            }
        } else if (currentView == firstView) {
            if (firstView.center.x > -self.frame.size.width * 0.6) {
                [self showPrevious];
            } else {
                [self resetViews];
            }
        }
        
    }
}

- (void)showPrevious {
    printf("\nshowPrevious:fv.x:%f", firstView.center.x);
    if (firstView != nil && secondView != nil) {
        [self applyShadow:firstView];
        CGAffineTransform rot = CGAffineTransformMakeRotation(-M_PI_4);
        //firstView.transform = rot;
        [UIView animateWithDuration:0.4 animations:^{
            firstView.center = CGPointMake(self.frame.size.width / 2, firstView.center.y);
            firstView.transform = CGAffineTransformMakeRotation(0.0);
            //thirdView.center = CGPointMake(self.bounds.size.width / 2, thirdView.center.y);
        } completion:^(BOOL finished) {
            [self removeShadow:firstView];
            //currentView = nil;
            [self getPrevious];
            
        }];
    } else {
        [self resetViews];
    }
}

/*
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    return;
    UITouch *t = [touches anyObject];
    UIView *tView = [t view];
    touchPoint = [t locationInView:self];
    dif = CGPointMake(tView.center.x - touchPoint.x, tView.center.y - touchPoint.y);
    
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    UITouch *t = [touches anyObject];
    
    CGPoint p = [t locationInView:[t view]];
    
    if (currentView == nil) {
        if (p.x < touchPoint.x) {
            direction = -1;
            if (thirdView != nil) {
                currentView = secondView;
            }
            currentViewOrigin = currentView.center;
            
        } else {
            direction = +1;
            if (firstView != nil) {
                currentView = firstView;
            }
            currentViewOrigin = currentView.center;
            dif = CGPointMake(firstView.center.x - touchPoint.x, firstView.center.y - touchPoint.y);
            
        }
        [self applyShadow:currentView];
        
    }
    [currentView touchesBegan:touches withEvent:event];
    if (fabs(p.x - touchPoint.x) < 100.0)
        return;
    
    p = [t locationInView:self];
    
    if (direction < 0) {
        //if (thirdView != nil) {
            currentView.center = CGPointMake(p.x + dif.x, currentView.center.y);
        //}
    } else {
        currentView.center = CGPointMake(p.x + dif.x, currentView.center.y);
    }
    
    printf("\np.x:%f, dif.x:%f, currentview:%p, currentView.x:%f", p.x, dif.x, currentView, currentView.center.x);
    
    
    
    
}
*/

- (void)dealloc {
    [self removeGestureRecognizer:lSwipe];
    [self removeGestureRecognizer:rSwipe];
    [self removeGestureRecognizer:pan];
    [firstView removeFromSuperview];
    firstView = nil;
    [secondView removeFromSuperview];
    secondView = nil;
    [thirdView removeFromSuperview];
    thirdView = nil;
}

- (void)applyShadow:(UIView *)view {
    view.layer.shadowColor = [[UIColor blackColor] CGColor];
    view.layer.shadowRadius = 10.0;
    view.layer.shadowOpacity = 0.75;
    view.clipsToBounds = NO;
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(4.0, 0.0);
}

- (void)removeShadow:(UIView *)view {
    view.layer.shadowColor = [[UIColor blackColor] CGColor];
    view.layer.shadowRadius = 0.0;
    view.layer.shadowOpacity = 0.0;
    view.clipsToBounds = YES;
    view.layer.masksToBounds = YES;
    view.layer.shadowOffset = CGSizeMake(0.0, 0.0);
}

/*
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    if (currentView != nil) {
        if (currentView == secondView && currentView.center.x < self.frame.size.width * 0.7) {
            [self showNext];
        } else if (currentView == firstView && currentView.center.x > -self.frame.size.width * 0.3) {
            [self showPrevious];
        } else {
            [self resetCurrentView];
        }
        
        currentView = nil;
        currentViewOrigin = CGPointZero;
        dif = CGPointZero;
        
    }
    

}
*/

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    BOOL b = [super pointInside:point withEvent:event];
    return b;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    return [super hitTest:point withEvent:event];
}
@end

@implementation CTWStackViewOverlay
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    //[self.nextResponder touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    //[self.nextResponder touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    //[self.nextResponder touchesEnded:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    //[self.nextResponder touchesCancelled:touches withEvent:event];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    return [super hitTest:point withEvent:event];
}
@end
